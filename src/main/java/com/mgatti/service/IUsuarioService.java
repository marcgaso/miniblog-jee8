package com.mgatti.service;

import java.util.List;

import com.mgatti.model.Usuario;
import com.mgatti.model.UsuarioRol;

public interface IUsuarioService extends IService<Usuario>{
	
	Usuario login(Usuario us);
	
	List<Usuario> leerUsuarios(String usuario);
}
