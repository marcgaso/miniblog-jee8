package com.mgatti.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Named;

import com.mgatti.dao.IUsuarioRolDAO;
import com.mgatti.model.Rol;
import com.mgatti.model.Usuario;
import com.mgatti.model.UsuarioRol;
import com.mgatti.service.IUsuarioRolService;

@Named
public class UsuarioRolServiceImpl implements IUsuarioRolService, Serializable {

	@EJB // @Inject
	private IUsuarioRolDAO dao;

	@Override
	public Integer registrar(UsuarioRol usuarioRol) throws Exception {		
		return dao.registrar(usuarioRol);
	}

	@Override
	public Integer modificar(UsuarioRol usuarioRol) throws Exception {		
		return dao.modificar(usuarioRol);
	}

	@Override
	public Integer eliminar(UsuarioRol usuarioRol) throws Exception {
		return dao.eliminar(usuarioRol);
	}

	@Override
	public List<UsuarioRol> listar() throws Exception {
		return dao.listar();
	}

	@Override
	public UsuarioRol listarPorId(UsuarioRol usuarioRol) throws Exception {
		return dao.listarPorId(usuarioRol);
	}

	@Override
	public List<UsuarioRol> listarRolesUsuario(Usuario u) throws Exception {
		return dao.listarRolesUsuario(u);
	}

	@Override
	public List<UsuarioRol> listarUsuariosRol(Rol r) throws Exception {
		return dao.listarUsuariosRol(r);
	}

	@Override
	public void asignarRolesUsuario(Usuario usuario, List<Rol> roles) throws Exception {
		// Ejemplo como esta capa sirve para no tocar la capa DAO con l�gica
		List<UsuarioRol> rolesUsuario = new ArrayList<>();
		
		roles.forEach(r->{
			UsuarioRol ur = new UsuarioRol();
			ur.setUsuario(usuario);
			ur.setRol(r);
			rolesUsuario.add(ur);
		});
		
		dao.asignarRolesUsuario(rolesUsuario);		
	}

}
