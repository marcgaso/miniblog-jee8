package com.mgatti.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Named;

import org.mindrot.jbcrypt.BCrypt;

import com.mgatti.dao.IUsuarioDAO;
import com.mgatti.model.Usuario;
import com.mgatti.service.IUsuarioService;

@Named
public class UsuarioServiceImpl implements IUsuarioService, Serializable {
	
	@EJB
	private IUsuarioDAO dao; 
	
	@Override
	public Usuario login(Usuario us) {
		Usuario usuarioBD = new Usuario(); 
		boolean login = false;
		
		try {
			usuarioBD = dao.leerUsuario(us);
			if (BCrypt.checkpw(us.getContraseña(), usuarioBD.getContraseña())) 
				return usuarioBD;
		}catch(Exception e) {
			throw e;
		}
		return new Usuario(); 
	}

	@Override
	public Integer registrar(Usuario us) throws Exception {
		return dao.registrar(us);
	}

	@Override
	public Integer modificar(Usuario us) throws Exception {
		return dao.modificar(us);
	}

	@Override
	public Integer eliminar(Usuario us) throws Exception {
		return dao.eliminar(us);
	}

	@Override
	public List<Usuario> listar() throws Exception {
		return dao.listar();
	}

	@Override
	public Usuario listarPorId(Usuario us) throws Exception {
		return dao.listarPorId(us);
	}

	@Override
	public List<Usuario> leerUsuarios(String usuario) {
		return dao.leerUsuarios(usuario);
	}
	
	
}
