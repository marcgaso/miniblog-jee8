package com.mgatti.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Named;

import com.mgatti.dao.IPublicacionDAO;
import com.mgatti.model.Persona;
import com.mgatti.model.Publicacion;
import com.mgatti.service.IPublicacionService;

@Named
public class PublicacionServiceImpl implements IPublicacionService, Serializable {

	@EJB
	private IPublicacionDAO dao;

	@Override
	public Integer registrar(Publicacion t) throws Exception {
		return dao.registrar(t);
	}

	@Override
	public Integer modificar(Publicacion t) throws Exception {
		return dao.modificar(t);
	}

	@Override
	public Integer eliminar(Publicacion t) throws Exception {
		return dao.eliminar(t);
	}

	@Override
	public List<Publicacion> listar() throws Exception {
		return dao.listar();
	}

	@Override
	public Publicacion listarPorId(Publicacion t) throws Exception {
		return dao.listarPorId(t);
	}

	@Override
	public List<Publicacion> listarPublicaciones(Persona publicador) throws Exception {
		return dao.listarPublicaciones(publicador);
	}

}
