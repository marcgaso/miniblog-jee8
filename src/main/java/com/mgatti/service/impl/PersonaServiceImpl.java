package com.mgatti.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.inject.Named;

import com.mgatti.dao.IPersonaDAO;
import com.mgatti.model.Persona;
import com.mgatti.service.IPersonaService;

@Named
public class PersonaServiceImpl implements IPersonaService, Serializable {
	//De esta capa service nos conectaremos a la capa DAO
	
	@EJB //@Inject
	private IPersonaDAO dao;	

	@Override
	public Integer registrar(Persona persona) throws Exception {
		return dao.registrar(persona);
	}

	@Override
	public Integer modificar(Persona persona) throws Exception {
		// TODO Auto-generated method stub
		return dao.modificar(persona);
	}

	@Override
	public Integer eliminar(Persona persona) throws Exception {
		return dao.eliminar(persona);
	}

	@Override
	public List<Persona> listar() throws Exception {
		return dao.listar();
	}

	@Override
	public Persona listarPorId(Persona persona) throws Exception {
		return dao.listarPorId(persona);
	}

	@Override
	public List<Persona> listarUsuariosValidos() {
		List<Persona> usuariosValidos = new ArrayList<>();
		try {
			usuariosValidos =  listar().stream()
											.filter(line-> Objects.nonNull(line.getUsuario()) )
											.filter(line->!line.getUsuario().toString().isEmpty() )
											.collect(Collectors.toList());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return usuariosValidos;
	}

}
