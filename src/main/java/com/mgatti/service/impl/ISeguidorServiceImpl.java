package com.mgatti.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Named;

import com.mgatti.dao.ISeguidorDAO;
import com.mgatti.model.Persona;
import com.mgatti.model.PublicadorSeguidor;
import com.mgatti.service.ISeguidorService;

@Named
public class ISeguidorServiceImpl implements Serializable, ISeguidorService {
	
	@EJB
	ISeguidorDAO dao;
	
	@Override
	public Integer registrarPublicadoresSeguidores(List<Persona> seguidores, List<Persona> publicadores) {
		List<PublicadorSeguidor> publicadoresSeguidores = this.buildPublicadoresSeguidores(seguidores,publicadores);
		try {
			dao.registrarPublicadoresSeguidores(publicadoresSeguidores);
		}catch(Exception e) {
			return 0;
		}
		return 1;
	}

	@Override
	public List<PublicadorSeguidor> listarSeguidores(Persona persona) {
		return dao.listarSeguidores(persona);
	}

	@Override
	public Integer dejarSeguir(List<Persona> seguidores, List<Persona> publicadores) {
		List<PublicadorSeguidor> publicadoresSeguidores = this.buildPublicadoresSeguidores(seguidores,publicadores);
		try {
			dao.dejarSeguir(publicadoresSeguidores);
		}catch(Exception e) {
			return 0;
		}
		return 1;
	}

	@Override
	public List<PublicadorSeguidor> listarSeguidos(Persona persona) {
		return dao.listarSeguidos(persona);
	}

	private List<PublicadorSeguidor> buildPublicadoresSeguidores(List<Persona> seguidores, List<Persona> publicadores){
		List<PublicadorSeguidor> publicadoresSeguidores = new ArrayList<>();
		publicadores.forEach(p->{
			seguidores.forEach(s->{
				PublicadorSeguidor ps = new PublicadorSeguidor(p,s);
				publicadoresSeguidores.add(ps);
			});
		});
		return publicadoresSeguidores;
	}
	

}
