package com.mgatti.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Named;

import com.mgatti.dao.IRolDAO;
import com.mgatti.model.Rol;
import com.mgatti.service.IRolService;

@Named
public class RolServiceImpl implements IRolService, Serializable {

	@EJB
	private IRolDAO dao;
	
	@Override
	public Integer registrar(Rol rol) throws Exception {		
		return dao.registrar(rol);
	}

	@Override
	public Integer modificar(Rol rol) throws Exception {
		return dao.modificar(rol);
	}

	@Override
	public Integer eliminar(Rol rol) throws Exception {
		return dao.eliminar(rol);
	}

	@Override
	public List<Rol> listar() throws Exception {
		return dao.listar();
	}

	@Override
	public Rol listarPorId(Rol rol) throws Exception {
		return dao.listarPorId(rol);
	}

}
