package com.mgatti.service;

import java.util.List;

import com.mgatti.model.Persona;
import com.mgatti.model.Publicacion;

public interface IPublicacionService extends IService<Publicacion> {

	List<Publicacion> listarPublicaciones(Persona publicador) throws Exception;

}
