package com.mgatti.service;

import java.util.List;

import com.mgatti.model.Persona;

public interface IPersonaService extends IService<Persona>{
	//Aqui definimos l�gica de negocio
	List<Persona> listarUsuariosValidos();
}
