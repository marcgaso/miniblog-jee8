package com.mgatti.service;

import java.util.List;

import com.mgatti.model.Persona;
import com.mgatti.model.PublicadorSeguidor;

public interface ISeguidorService {

	Integer registrarPublicadoresSeguidores(List<Persona> seguidores, List<Persona> publicadores);

	List<PublicadorSeguidor> listarSeguidores(Persona persona);
	
	Integer dejarSeguir(List<Persona> seguidores, List<Persona> publicadores);
	
	List<PublicadorSeguidor> listarSeguidos(Persona persona);

}
