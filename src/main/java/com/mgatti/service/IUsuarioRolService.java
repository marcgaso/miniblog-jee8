package com.mgatti.service;

import java.util.List;

import com.mgatti.model.Rol;
import com.mgatti.model.Usuario;
import com.mgatti.model.UsuarioRol;

public interface IUsuarioRolService extends IService<UsuarioRol> {
	
	public List<UsuarioRol> listarRolesUsuario(Usuario u) throws Exception;

	public List<UsuarioRol> listarUsuariosRol(Rol r) throws Exception;

	public void asignarRolesUsuario(Usuario usuario, List<Rol> roles) throws Exception;
}
