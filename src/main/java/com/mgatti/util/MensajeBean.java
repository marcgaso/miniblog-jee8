package com.mgatti.util;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

//En este caso se usa RequestScoped porque no me interesa que guarde el estado anterior de la transaccion. Instancia independiente por cada petici�n
@Named
@RequestScoped
public class MensajeBean implements Serializable {
	private void mostrarMensaje(String titulo, String cuerpo, String severidad) {
		switch (severidad) {
		case "INFO":
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, titulo, cuerpo));
			break;
		case "WARN":
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_WARN, titulo, cuerpo));
			break;
		case "ERROR":
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, titulo, cuerpo));
			break;
		case "FATAL":
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_FATAL, titulo, cuerpo));
			break;
		}
	}
	
	public void info(String titulo, String cuerpo) {
		mostrarMensaje(titulo, cuerpo, "INFO"); 
	}
	
	public void warn(String titulo, String cuerpo) {
		mostrarMensaje(titulo, cuerpo, "WARN"); 
	}
	
	public void error(String titulo, String cuerpo) {
		mostrarMensaje(titulo, cuerpo, "ERROR"); 
	}
	
	public void fatal(String titulo, String cuerpo) {
		mostrarMensaje(titulo, cuerpo, "FATAL"); 
	}
	
	
}
