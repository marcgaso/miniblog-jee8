package com.mgatti.dao;

import java.util.List;

import javax.ejb.Local;

import com.mgatti.model.Rol;
import com.mgatti.model.Usuario;
import com.mgatti.model.UsuarioRol;

@Local
public interface IUsuarioRolDAO extends ICRUD<UsuarioRol> {
	
	public List<UsuarioRol> listarRolesUsuario(Usuario u) throws Exception;

	public List<UsuarioRol> listarUsuariosRol(Rol r) throws Exception;

	public void asignarRolesUsuario(List<UsuarioRol> usuarioRoles) throws Exception;
}
