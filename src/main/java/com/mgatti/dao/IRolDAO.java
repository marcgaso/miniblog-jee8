package com.mgatti.dao;

import javax.ejb.Local;

import com.mgatti.model.Rol;

@Local
public interface IRolDAO extends ICRUD<Rol>{
}
