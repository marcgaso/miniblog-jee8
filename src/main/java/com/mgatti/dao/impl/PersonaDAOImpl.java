package com.mgatti.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.mgatti.dao.IPersonaDAO;
import com.mgatti.model.Persona;
import com.mgatti.model.Usuario;

//Con el annotation @Named le indicamos al contenedor de CDI de JEE que reserve una instancia para esta clase
//@Stateless notaci�n de EJB. Permitir� manejo de transacciones en BD
@Stateless //@Named
public class PersonaDAOImpl implements IPersonaDAO, Serializable {
//Capa de interacci�n con BD donde se utilizar� inyecci�n de dependencias.
	
	@PersistenceContext(unitName = "blogPU")
	private EntityManager em;
	
	@Override
	public Integer registrar(Persona persona) throws Exception {
		try {
			em.persist(persona);
		}catch(Exception e) {
			e.printStackTrace();
			throw e;
		}

		System.out.println(persona.getId()+" "+persona.getNombres() + " " + persona.getApellidos() + " " + persona.getSexo() + " " + persona.getPais() + " " + persona.getDireccion());
		
		return persona.getId();
	}

	@Override
	public Integer modificar(Persona persona) throws Exception {
		try {			
			em.merge(persona);
		}catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
		return persona.getId();
	}

	@Override
	public Integer eliminar(Persona persona) throws Exception {
		em.remove(em.merge(persona));
		return 1;
	}

	@Override
	public List<Persona> listar() throws Exception {
		Query q = em.createQuery("FROM Persona u");

		List<Persona> personas = (List<Persona>) q.getResultList();

		return personas;
	}

	@Override
	public Persona listarPorId(Persona persona) throws Exception {
		Query q = em.createQuery("FROM Persona p WHERE p.id = ?1");
		q.setParameter(1, persona.getId());

		List<Persona> personas = (List<Persona>) q.getResultList();

		return (personas != null && !personas.isEmpty()) ? personas.get(0) : new Persona();
	}

}
