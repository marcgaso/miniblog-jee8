package com.mgatti.dao.impl;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import com.mgatti.dao.IUsuarioRolDAO;
import com.mgatti.model.Rol;
import com.mgatti.model.Usuario;
import com.mgatti.model.UsuarioRol;

//Con el annotation @Named le indicamos al contenedor de CDI de JEE que reserve una instancia para esta clase
//@Stateless notación de EJB. Permitirá manejo de transacciones en BD
@Stateless //@Named
public class UsuarioRolDAOImpl implements IUsuarioRolDAO, Serializable {

	@PersistenceContext(unitName = "blogPU")
	private EntityManager em;

	@Override
	public Integer registrar(UsuarioRol t) throws Exception {
		em.persist(t);
		return t.getId();
	}

	@Override
	public Integer modificar(UsuarioRol t) throws Exception {
		em.merge(t);
		return t.getId();
	}

	@Override
	public Integer eliminar(UsuarioRol t) throws Exception {
		em.remove(em.merge(t));
		return 1;
	}

	@Override
	public List<UsuarioRol> listar() throws Exception {
		Query q = em.createQuery("FROM UsuarioRol ur");

		List<UsuarioRol> usuariosRoles = (List<UsuarioRol>) q.getResultList();

		return usuariosRoles;
	}

	@Override
	public UsuarioRol listarPorId(UsuarioRol t) throws Exception {
		Query q = em.createQuery("FROM UsuarioRol ur WHERE ur.id = ?1");
		q.setParameter(1, t.getId());

		List<UsuarioRol> usuariosRoles = (List<UsuarioRol>) q.getResultList();

		return (usuariosRoles != null && !usuariosRoles.isEmpty()) ? usuariosRoles.get(0) : new UsuarioRol();
	}

	@Override
	public List<UsuarioRol> listarRolesUsuario(Usuario u) throws Exception {
		Query q = em.createQuery("FROM UsuarioRol ur WHERE ur.usuario.id = ?1");
		q.setParameter(1, u.getId());

		List<UsuarioRol> rolesUsuario = (List<UsuarioRol>) q.getResultList();

		return rolesUsuario;
	}

	@Override
	public List<UsuarioRol> listarUsuariosRol(Rol r) throws Exception {
		Query q = em.createQuery("FROM UsuarioRol ur WHERE ur.rol.id = ?1");
		q.setParameter(1, r.getId());

		List<UsuarioRol> usuariosRol = (List<UsuarioRol>) q.getResultList();

		return usuariosRol;
	}

	@Override
	@Transactional
	public void asignarRolesUsuario(List<UsuarioRol> usuarioRoles) throws Exception {
		//Borramos los roles que pueda tener previamente asignado el usuario
		Query q = em.createNativeQuery("DELETE usuarios_roles WHERE id_usuario = ?1");
		q.setParameter(1, usuarioRoles.get(0).getUsuario().getId());
		q.executeUpdate();
		
		int i = 0;
		for(UsuarioRol ur : usuarioRoles) {
			if (i%100==0) {
				em.flush();
				em.clear();
			}
			this.registrar(ur);
			i++;
		}
	}
}
