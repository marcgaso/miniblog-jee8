package com.mgatti.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.mgatti.dao.IPublicacionDAO;
import com.mgatti.model.Persona;
import com.mgatti.model.Publicacion;
import com.mgatti.model.PublicadorSeguidor;

@Stateless
public class PublicacionDAOImpl implements Serializable, IPublicacionDAO {

	@PersistenceContext(unitName = "blogPU")
	private EntityManager em;
	
	@Override
	public Integer registrar(Publicacion t) throws Exception {
		em.persist(t);
		em.flush();
		return t.getId();
	}

	@Override
	public Integer modificar(Publicacion t) throws Exception {
		em.merge(t);
		em.flush();
		return t.getId();
	}

	@Override
	public Integer eliminar(Publicacion t) throws Exception {
		em.remove(em.merge(t));
		return 1;
	}

	@Override
	public List<Publicacion> listar() throws Exception {
		Query q = em.createQuery("FROM Publicacion p");
		
		List<Publicacion> publicaciones = (List<Publicacion>) q.getResultList();
		
		return (publicaciones);
	}

	@Override
	public Publicacion listarPorId(Publicacion t) throws Exception {
		Query q = em.createQuery("FROM Publicacion p WHERE p.id = ?1");
		q.setParameter(1, t.getId());
		
		List<Publicacion> publicaciones = (List<Publicacion>) q.getResultList();
		
		return publicaciones!=null && !publicaciones.isEmpty() ? publicaciones.get(0) : new Publicacion();
	}

	@Override
	public List<Publicacion> listarPublicaciones(Persona publicador) throws Exception {
		Query q = em.createQuery("FROM Publicacion p WHERE p.publicador.id = ?1");
		q.setParameter(1, publicador.getId());

		List<Publicacion> PublicacionesDePublicador = (List<Publicacion>) q.getResultList();

		return PublicacionesDePublicador;
	}
	

}
