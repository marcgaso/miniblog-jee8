package com.mgatti.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.mgatti.dao.IUsuarioDAO;
import com.mgatti.model.Usuario;

@Stateless
public class UsuarioDAOImpl implements IUsuarioDAO, Serializable {
	
	private final static Logger LOGGER = Logger.getLogger(UsuarioDAOImpl.class.getName());
	
	@PersistenceContext(unitName = "blogPU")
	private EntityManager em;

	@Override
	public Integer registrar(Usuario usuario) throws Exception {
		em.persist(usuario);
		return usuario.getId();
	}

	@Override
	public Integer modificar(Usuario usuario) throws Exception {
		em.merge(usuario);
		return usuario.getId();
	}

	@Override
	public Integer eliminar(Usuario usuario) throws Exception {
		em.remove(em.merge(usuario));
		return 1;
	}

	@Override
	public List<Usuario> listar() throws Exception {
		Query q = em.createQuery("FROM Usuario u");

		List<Usuario> usuarios = (List<Usuario>) q.getResultList();

		return usuarios;
	}

	@Override
	public Usuario listarPorId(Usuario usuario) throws Exception {
		Query q = em.createQuery("FROM Usuario u WHERE u.id = ?1");
		q.setParameter(1, usuario.getId());

		List<Usuario> usuarios = (List<Usuario>) q.getResultList();

		return (usuarios != null && !usuarios.isEmpty()) ? usuarios.get(0) : new Usuario();
	}

	@Override
	public Usuario leerUsuario(Usuario usuario) {
		Usuario us = new Usuario();
		try {
			Query q = em.createQuery("FROM Usuario u WHERE u.usuario = ?1");
			q.setParameter(1, usuario.getUsuario());
			
			List<Usuario> usuarios = (List<Usuario>) q.getResultList();
			
			if(!usuarios.isEmpty())
				us = usuarios.get(0);
		}catch(Exception e) {
			throw e;
		}
		System.out.println("usuario: "+us.getUsuario()+" - clave: "+us.getContraseña());
		return us; 
	}

	@Override
	public List<Usuario> leerUsuarios(String usuario) {
		List<Usuario> usuarios = new ArrayList<>();
		try {
			Query q = em.createQuery("FROM Usuario u WHERE u.usuario like ?1");
			q.setParameter(1, usuario);
			
			usuarios = (List<Usuario>) q.getResultList();
			if(!usuarios.isEmpty())
				LOGGER.info("Cantidad de usuarios recuperados: "+usuarios.size());
		}catch(Exception e) {
			LOGGER.severe("Error "+e.getMessage());
			throw e;
		}
		return usuarios; 
	}
}
