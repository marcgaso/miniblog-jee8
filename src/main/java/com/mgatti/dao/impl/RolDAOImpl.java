package com.mgatti.dao.impl;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.mgatti.dao.IRolDAO;
import com.mgatti.model.Rol;

//Con el annotation @Named le indicamos al contenedor de CDI de JEE que reserve una instancia para esta clase
//@Stateless notaci�n de EJB. Permitir� manejo de transacciones en BD
@Stateless //@Named
public class RolDAOImpl implements IRolDAO, Serializable{
	
	@PersistenceContext(unitName = "blogPU")
	private EntityManager em;
	
	@Override
	public Integer registrar(Rol rol) throws Exception {
		em.persist(rol);
		return rol.getId();
	}

	@Override
	public Integer modificar(Rol rol) throws Exception {
		em.merge(rol);
		return rol.getId();
	}

	@Override
	public Integer eliminar(Rol rol) throws Exception {
		em.remove(em.merge(rol)); //Se remueve as� para evitar eliminar algo que no est� en la BD.
		return 1;
	}

	@Override
	public List<Rol> listar() throws Exception {
		// JPQL - Java Persistence Query Language
		// En JPQL no se consulta la tabla sin� la entidad (Clase)
		Query q = em.createQuery("SELECT r FROM Rol r");
		
		List<Rol> roles = (List<Rol>) q.getResultList();
		
		return (roles); //Se recomienda un casteo explicito para evitar que retorne una List<Object>
	}

	@Override
	public Rol listarPorId(Rol rol) throws Exception {
		Query q = em.createQuery("FROM Rol r WHERE r.id = ?1");
		q.setParameter(1, rol.getId());
		
		List<Rol> roles = (List<Rol>) q.getResultList();
		
		return roles!=null && !roles.isEmpty() ? roles.get(0) : new Rol();

	}

}
