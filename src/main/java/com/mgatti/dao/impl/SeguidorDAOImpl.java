package com.mgatti.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.mgatti.dao.ISeguidorDAO;
import com.mgatti.model.Persona;
import com.mgatti.model.PublicadorSeguidor;

@Stateless
public class SeguidorDAOImpl implements Serializable, ISeguidorDAO {
	
	@PersistenceContext(unitName = "blogPU")
	private EntityManager em;

	@Override
	public Integer registrarPublicadoresSeguidores(List<PublicadorSeguidor> publicadoresSeguidores) {
		try {
			//Como las lambdas tiene su propio scope se define como arreglo para que sea reconocido este contador
			int[] i = {0};
			publicadoresSeguidores.forEach(ps->{
				em.persist(ps);
				//cada cierta cantidad de registro se realiza un flush
				if(i[0]%100==0) {
					em.flush();
					em.clear();
				}
				i[0]++;
			});
		}catch(Exception e) {
			return 0;
		}
		return 1;
	}

	@Override
	public List<PublicadorSeguidor> listarSeguidores(Persona persona) {
		List<PublicadorSeguidor> seguidores = new ArrayList<>();
		try {
			Query q = em.createQuery("FROM PublicadorSeguidor p WHERE p.publicador.id = ?1");
			q.setParameter(1, persona.getId());
			
			seguidores = (List<PublicadorSeguidor>) q.getResultList();
			
		}catch(Exception e) {
			
		}
		return seguidores;
	}

	@Override
	public Integer dejarSeguir(List<PublicadorSeguidor> publicadoresSeguidores) {
		int rpta = 0;
		try {
			publicadoresSeguidores.forEach(ps->{
				Query q = em.createQuery(
						"DELETE FROM PublicadorSeguidor WHERE publicador.id =?1 AND seguidor.id =?2");
				q.setParameter(1, ps.getPublicador().getId());
				q.setParameter(2, ps.getSeguidor().getId());
				
				q.executeUpdate();
			});
			rpta = 1;
		}catch(Exception e) {
			rpta = 0;
		}
		return rpta;
	}

	@Override
	public List<PublicadorSeguidor> listarSeguidos(Persona persona) {
		List<PublicadorSeguidor> seguidos = new ArrayList<>();
		try {
			Query q = em.createQuery("FROM PublicadorSeguidor p WHERE p.seguidor.id = ?1");
			q.setParameter(1, persona.getId());
			
			seguidos = (List<PublicadorSeguidor>) q.getResultList();
			
		}catch(Exception e) {
			
		}
		return seguidos;
	}

}
