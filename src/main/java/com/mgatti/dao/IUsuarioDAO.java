package com.mgatti.dao;

import java.util.List;

import javax.ejb.Local;

import com.mgatti.model.Usuario;

@Local
public interface IUsuarioDAO extends ICRUD<Usuario> {

	Usuario leerUsuario(Usuario usuario);
	
	List<Usuario> leerUsuarios(String usuario);
	
}
