package com.mgatti.dao;

import java.util.List;

import javax.ejb.Local;

import com.mgatti.model.Persona;

@Local // When used on the bean class, declares the local business interface(s) for a
		// session bean. When used on an interface, designates that interface as a local
		// business interface. In this case, no value element should be provided.
		// https://docs.oracle.com/javaee/6/api/javax/ejb/Local.html

public interface IPersonaDAO extends ICRUD<Persona> {

}
