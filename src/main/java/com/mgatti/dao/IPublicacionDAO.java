package com.mgatti.dao;

import java.util.List;

import javax.ejb.Local;

import com.mgatti.model.Persona;
import com.mgatti.model.Publicacion;

@Local
public interface IPublicacionDAO extends ICRUD<Publicacion> {

	List<Publicacion> listarPublicaciones(Persona publicador) throws Exception;

}
