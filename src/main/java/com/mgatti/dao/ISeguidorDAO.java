package com.mgatti.dao;

import java.util.List;

import javax.ejb.Local;

import com.mgatti.model.Persona;
import com.mgatti.model.PublicadorSeguidor;

@Local
public interface ISeguidorDAO {

	Integer registrarPublicadoresSeguidores(List<PublicadorSeguidor> publicadoresSeguidores);

	List<PublicadorSeguidor> listarSeguidores(Persona persona);
	
	Integer dejarSeguir(List<PublicadorSeguidor> publicadoresSeguidores);
	
	List<PublicadorSeguidor> listarSeguidos(Persona persona);

}
