package com.mgatti.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mgatti.model.Publicacion;
import com.mgatti.model.PublicadorSeguidor;
import com.mgatti.model.Usuario;
import com.mgatti.service.IPublicacionService;
import com.mgatti.service.ISeguidorService;

@Named
@ViewScoped
public class PrincipalBean implements Serializable {

	@Inject
	private IPublicacionService publicacionService;
	@Inject
	private ISeguidorService seguidorService;
	
	private List<Publicacion> publicaciones;
	private Usuario usuario;
	
	@PostConstruct
	private void init() {
		this.usuario = (Usuario)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
		this.publicaciones = new ArrayList<>();
		this.listarPublicacionesDeMisSeguidores();
	}

	public void listarPublicacionesDeMisSeguidores() {
		System.out.println("MAG listarPublicacionesDeMisSeguidores");
		try {
			List<PublicadorSeguidor> seguidores = this.seguidorService.listarSeguidores(this.usuario.getPersona());
			seguidores.forEach(seguidor->{
				try {
					List<Publicacion> publicacionesSeguidor = this.publicacionService.listarPublicaciones(seguidor.getSeguidor());
					this.publicaciones.addAll(publicacionesSeguidor);
				} catch (Exception e) {
					e.printStackTrace();
				}				
			});
		}catch(Exception e) {
			
		}
	}

	public List<Publicacion> getPublicaciones() {
		return publicaciones;
	}

	public void setPublicaciones(List<Publicacion> publicaciones) {
		this.publicaciones = publicaciones;
	}	
	
	
}
