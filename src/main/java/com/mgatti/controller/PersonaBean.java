package com.mgatti.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import com.mgatti.model.Persona;
import com.mgatti.service.IPersonaService;

@Named
@ViewScoped
public class PersonaBean implements Serializable {
	//Capa controller -> capa service -> capa DAO
	
	@Inject
	private IPersonaService service;
	
	private Persona persona;
	private List<Persona> personas;
	
	private String tipoDialog;
	
	//La teor�a dice que la inyecci�n de dependencias no est� disponible al momento de la construcci�n del objeto.
	@PostConstruct
	public void init() {
		this.persona = new Persona();
		this.listar();
		personas.forEach(x->{
			System.out.println(x.getId()+" "+x.getNombres() + " " + x.getApellidos() + " " + x.getSexo() + " " + x.getPais() + " " + x.getDireccion());
		});
	}
	
	public void registrar() {
		//Se invoca al metodo registrar del service que este luego invocar� al DAO
		try {
			this.service.registrar(this.getPersona());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void handleFileUpload(FileUploadEvent event) {
		UploadedFile file = event.getFile();
        FacesMessage msg = new FacesMessage("Succesful", file.getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, msg);
		this.persona.setFoto(file.getContents());
    }
		
	public void operar(String accion) {		
		try {
			if(accion.equalsIgnoreCase("R")) {
				this.service.registrar(this.persona);
			}
			else if(accion.equalsIgnoreCase("M"))
				this.service.modificar(this.persona);
			this.listar();
		}catch(Exception e) {
			e.printStackTrace();
		}		
	}
	
	public void mostrarData(Persona p) {
		this.persona = p;		
		this.tipoDialog = "Modificar Persona";
	}
	
	public void limpiarControles() {
		this.persona = new Persona();
		this.tipoDialog = "Nueva Persona";
	}
	
	
	public void listar() {
		try {
			this.setPersonas(this.service.listar());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Getter & Setters
	 * 
	 */

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}	
	
	
	public List<Persona> getPersonas() {
		return personas;
	}

	public void setPersonas(List<Persona> personas) {
		this.personas = personas;
	}

	public String getTipoDialog() {
		return tipoDialog;
	}
		
}
