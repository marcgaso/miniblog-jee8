package com.mgatti.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

import com.mgatti.model.Usuario;
import com.mgatti.service.IUsuarioService;

@Named
@ViewScoped
public class UsuarioBean implements Serializable {

	private final static Logger LOGGER = Logger.getLogger(UsuarioBean.class.getName());
	
	private String usuarioBusqueda;
	private List<Usuario> usuarios;
	private Usuario usuarioSeleccionado;
	private boolean verificacionOK;
	String tituloVentanaUsuario;
	@Inject
	IUsuarioService service;
	
	@PostConstruct
	public void init() {
		this.verificacionOK = false;
		this.usuarioBusqueda = "%";
		this.usuarioSeleccionado = new Usuario();
		this.recuperarUsuarios();
	}
		
	public void recuperarUsuarios() {
		try {
			this.usuarios = this.service.leerUsuarios(this.usuarioBusqueda);
		}catch(Exception e) {
			LOGGER.severe(e.getMessage());
		}
	}
	
	public void onRowSelect(SelectEvent event) {
        FacesMessage msg = new FacesMessage("Usuario seleccionado", ((Usuario) event.getObject()).getUsuario());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
 
    public void onRowUnselect(UnselectEvent event) {
        FacesMessage msg = new FacesMessage("Usuario des-seleccionado", ((Usuario) event.getObject()).getUsuario());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
	
    public void requiredSelect() {
        if (Objects.isNull(this.usuarioSeleccionado)) {
        	FacesMessage msg = new FacesMessage( FacesMessage.SEVERITY_ERROR, "Error", "No seleccionó usuario");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }
    
    public void verificar() {
    	try {
			Usuario usuario = service.login(this.usuarioSeleccionado);
			if (usuario!=null && usuario.getPersona()!=null){
				this.verificacionOK = true;
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", "Credenciales correctas. Introduzca la nueva contraseña"));
			} else {
				this.verificacionOK = false;				
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_WARN, "Advertencia", "Credenciales incorrectas. No puede modificar la contraseña de este usuario"));
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_FATAL, "Aviso", e.getMessage()));
		}    	   	
    }
    
    public void initDialogUsuario() {
    	this.usuarioSeleccionado.setContraseña(null);
		this.tituloVentanaUsuario = "Modificando Usuario: " + usuarioSeleccionado.getUsuario(); 
		this.verificacionOK = false;
    }
    
    public void actualizarContraseña() {
    	try {
    		this.usuarioSeleccionado.hashContraseña();
			Integer rpta = service.modificar(this.usuarioSeleccionado);
			if (rpta!=null && rpta >= 0){
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", "Usuario modificado exitosamente"));
			} else {				
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_WARN, "Advertencia", "El usuario no fue modificado"));
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_FATAL, "Aviso", e.getMessage()));
		}
    }
    
	/**
	 * Getter & Setters
	 * 
	 */
	
	public String getUsuarioBusqueda() {
		return usuarioBusqueda;
	}

	public boolean isVerificacionOK() {
		return verificacionOK;
	}

	public void setVerificacionOK(boolean verificacionOK) {
		this.verificacionOK = verificacionOK;
	}

	public String getTituloVentanaUsuario() {
		return tituloVentanaUsuario;
	}

	public void setTituloVentanaUsuario(String tituloVentanaUsuario) {
		this.tituloVentanaUsuario = tituloVentanaUsuario;
	}

	public Usuario getUsuarioSeleccionado() {
		return usuarioSeleccionado;
	}

	public void setUsuarioSeleccionado(Usuario usuarioSeleccionado) {
		this.usuarioSeleccionado = usuarioSeleccionado;
	}

	public void setUsuarioBusqueda(String usuarioBusqueda) {
		this.usuarioBusqueda = usuarioBusqueda;
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	


}
