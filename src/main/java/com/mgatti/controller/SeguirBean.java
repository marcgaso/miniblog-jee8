package com.mgatti.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mgatti.model.Persona;
import com.mgatti.model.PublicadorSeguidor;
import com.mgatti.model.Usuario;
import com.mgatti.service.IPersonaService;
import com.mgatti.service.ISeguidorService;
import com.mgatti.util.MensajeBean;

@Named
@ViewScoped
public class SeguirBean implements Serializable {
	
	@Inject
	private IPersonaService personaService;
	@Inject
	private ISeguidorService seguidorService;
	@Inject
	private MensajeBean mensaje;
	
	private List<Persona> personas;
	private List<PublicadorSeguidor> seguidos;
	private Usuario usuario;
	
	@PostConstruct
	private void init() {		
		this.usuario = (Usuario)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
		
		this.listarSeguidos();
		this.listarUsuariosValidos();
	}
	
	public void listarUsuariosValidos() {
		this.personas = this.personaService.listarUsuariosValidos();
		
		this.personas.remove(this.usuario.getPersona());
		
		this.personas.forEach(p->{
			this.seguidos.forEach(s->{
				if(s.getPublicador().getId()==p.getId())
					p.setEsSeguido(true);
			});
		});
	}
	
	public void listarSeguidos() {
		try {
			this.seguidos = this.seguidorService.listarSeguidos(this.usuario.getPersona());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void follow(Persona aSeguir) {
		try {
			List<Persona> seguidores = new ArrayList<>();
			List<Persona> publicadores = new ArrayList<>();
			
			seguidores.add(this.usuario.getPersona());
			publicadores.add(aSeguir);
			
			this.seguidorService.registrarPublicadoresSeguidores(seguidores, publicadores);
			mensaje.info("AVISO", "�Ahora sigues a "+aSeguir.getNombres()+" "+aSeguir.getApellidos()+"!");
		}catch(Exception e) {
			mensaje.error("AVISO", "Error al seguir");
		}finally {
			this.listarSeguidos();
			this.listarUsuariosValidos();
		}
	}

	public void unfollow(Persona aDejar) {
		try {
			List<Persona> seguidores = new ArrayList<>();
			List<Persona> publicadores = new ArrayList<>();
			
			seguidores.add(this.usuario.getPersona());
			publicadores.add(aDejar);
			
			this.seguidorService.dejarSeguir(seguidores, publicadores);
			mensaje.info("AVISO", "�Dejaste de seguir a "+aDejar.getNombres()+" "+aDejar.getApellidos()+"!");
		}catch(Exception e) {
			mensaje.error("AVISO", "Error al dejar de seguir");
		}finally {
			this.listarSeguidos();
			this.listarUsuariosValidos();
		}
	}
	/**
	 * Getter & Setters
	 * 
	 */
	
	public List<Persona> getPersonas() {
		return personas;
	}

	public void setPersonas(List<Persona> personas) {
		this.personas = personas;
	}
	
	

}
