package com.mgatti.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import com.mgatti.model.Persona;
import com.mgatti.model.Rol;
import com.mgatti.model.Usuario;
import com.mgatti.service.IPersonaService;
import com.mgatti.service.IUsuarioRolService;

@Named
@ViewScoped //Da soporte a Ajax y en esta p�gina se va a utilizar. ViewScoped el estado anterior se mantiene siempre que sea en la misma p�gina
public class RegistrarBean implements Serializable{

	@Inject
	private IPersonaService personaService;
	@Inject
	private IUsuarioRolService usuarioRolService;
	
	private Persona persona;
	private Usuario usuario;
	
	@PostConstruct
	public void init() {
		this.persona = new Persona();
		this.usuario = new Usuario();
	}
	
	@Transactional //en caso de un error en cualquier punto hace rollback. Guarda integridad de datos.
	public void registrar() {
		try {
			this.usuario.hashContrase�a();
			this.persona.setUsuario(this.usuario);
			this.usuario.setPersona(this.persona);
			this.personaService.registrar(this.persona);
			
			/*
			Rol rol = new Rol();
			rol.setId(1);
			List<Rol> roles = new ArrayList<>();
			roles.add(rol);
			
			this.usuarioRolService.asignarRolesUsuario(this.usuario, roles);
			*/
		}catch(Exception e) {
			
		}
		
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
