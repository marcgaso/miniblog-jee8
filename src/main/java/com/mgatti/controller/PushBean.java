package com.mgatti.controller;
import java.io.Serializable;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.push.Push;
import javax.faces.push.PushContext;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@ApplicationScoped //Porque este socket se va a usar a lo largo de la aplicación
public class PushBean implements Serializable{

	@Inject 
	@Push(channel="notify") //notify es el nombre del canal que colocamos y por donde viajaran los datos 
	private PushContext push;
		
	public void sendMessage() {
		push.send("test");
	}
}
