package com.mgatti.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Objects;

import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mgatti.model.Persona;
import com.mgatti.service.IPersonaService;

// "/imagen/*" significa cualquier contenido despues de esa ruta
@WebServlet("/imagen/*")
public class ImageServlet extends HttpServlet implements Serializable {
	
	@Inject
	private IPersonaService service;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		byte [] foto = null;
		try {
			String captura= req.getPathInfo().substring(1); //en el request viajar� algo as� '/imagen/1'
			//System.out.println("ENTRO AL SERVLET :"+ captura);
			if(captura!=null && !captura.equalsIgnoreCase("")) {
				int id = Integer.parseInt(captura);
				Persona per = new Persona();
				per.setId(id);
				per = service.listarPorId(per);
				//System.out.println("SERVLET " + per.getId()+" "+per.getNombres() + " " + per.getApellidos() + " " + per.getSexo() + " " + per.getPais() + " " + per.getDireccion());
				if(per.getFoto()!=null) {
					foto = per.getFoto();
				}
				else {
					String path = this.getClass().getProtectionDomain().getCodeSource().getLocation().toURI().getPath()+"/../../resources/images/";
					if (per.isMale())
						path=path+"male64.png";
					else
						path=path+"female64.png";
					File file = new File(path);						
					BufferedImage bImage = ImageIO.read(file);						
				    ByteArrayOutputStream bos = new ByteArrayOutputStream();
				    ImageIO.write(bImage, "png", bos );
				    foto = bos.toByteArray();
				}
				if(Objects.nonNull(foto)) {
					resp.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); //HTTP 1.1
					resp.setHeader("Pragma", "no-cache"); //HTTP1.0
					resp.setDateHeader("Expires", 0); //Proxies
					
					resp.setContentType(getServletContext().getMimeType("image/jpg"));
					resp.setContentLength(foto.length);
					resp.getOutputStream().write(foto);	
				}			
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}	

}
