package com.mgatti.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mgatti.model.Publicacion;
import com.mgatti.model.Usuario;
import com.mgatti.service.IPublicacionService;

@Named
@ViewScoped
public class PublicacionBean implements Serializable {
	
	@Inject
	private IPublicacionService servicio;
	@Inject
	private PushBean push;
	
	private Publicacion publicacion;
	private List<Publicacion> publicaciones;
	
	@PostConstruct
	public void init() {
		Usuario usuario = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
		
		this.publicacion = new Publicacion();
		this.publicacion.setPublicador(usuario.getPersona());		
		
		this.listarPublicacionesDePublicador();
	}
	
	public void publicar() {
		try {
			this.publicacion.buildTagsMenciones();
			this.servicio.registrar(this.publicacion);
			this.push.sendMessage();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void listarPublicacionesDePublicador() {
		try {
			this.publicaciones = this.servicio.listarPublicaciones(this.publicacion.getPublicador());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * Getter & Setters
	 * 
	 */

	public Publicacion getPublicacion() {
		return publicacion;
	}

	public void setPublicacion(Publicacion publicacion) {
		this.publicacion = publicacion;
	}

	public List<Publicacion> getPublicaciones() {
		return publicaciones;
	}

	public void setPublicaciones(List<Publicacion> publicaciones) {
		this.publicaciones = publicaciones;
	}
	
	

}
