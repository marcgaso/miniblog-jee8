package com.mgatti.controller;

import java.io.IOException;
import java.io.Serializable;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import com.mgatti.model.Usuario;

@Named
@ViewScoped
public class MasterBean implements Serializable {
	
	
	
	public void verificarSesion() throws IOException {
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		String redireccion = "./../index.xhtml";
		try {
			Usuario us = (Usuario) ec.getSessionMap().get("usuario");
			if (us == null)
				ec.redirect(redireccion);
		}catch(Exception e) {
			ec.redirect(redireccion);
		}
	}
	
	public void cerrarSesion() {
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		ec.invalidateSession();
	}
}
