package com.mgatti.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.CellEditEvent;
import org.primefaces.event.RowEditEvent;

import com.mgatti.model.Rol;
import com.mgatti.service.IRolService;

@Named
@ViewScoped
public class RolBean implements Serializable {
	
	private List<Rol> roles;
	
	@Inject
	private IRolService rolService;
	
	@PostConstruct
	private void init() {
		this.listar();
	}
	
	public void listar() {
		try {
			roles = this.rolService.listar();
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}		
	}

	public List<Rol> getRoles() {
		return roles;
	}

	public void setRoles(List<Rol> roles) {
		this.roles = roles;
	}
	
	public void onRowEdit(RowEditEvent event) {
		try {
			Rol newRol = (Rol) event.getObject();
			Rol old = this.rolService.listarPorId(newRol);
			this.rolService.modificar(newRol);
	        FacesMessage msg = new FacesMessage("Rol Editado", "Se modific� "+old.getTipo()+" por "+ newRol.getTipo());
	        FacesContext.getCurrentInstance().addMessage(null, msg);
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
    }
     
    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edici�n Cancelada", ((Rol) event.getObject()).getTipo());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
     
    public void onCellEdit(CellEditEvent event) {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();
         
        if(newValue != null && !newValue.equals(oldValue)) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cell Changed", "Old: " + oldValue + ", New:" + newValue);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }
}
