package com.mgatti.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.mgatti.model.PublicadorSeguidor;
import com.mgatti.model.Usuario;
import com.mgatti.service.ISeguidorService;
import com.mgatti.util.MensajeBean;

@Named
@ViewScoped
public class MeSigueBean implements Serializable {

	@Inject
	private ISeguidorService seguidorService;
	@Inject
	private MensajeBean mensaje;
	
	private List<PublicadorSeguidor> seguidores;
	private Usuario usuario;
	
	@PostConstruct
	private void init() {
		this.usuario = (Usuario)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
		
		this.listarSeguidores();
	}
	
	private void listarSeguidores() {
		try {
			this.seguidores = this.seguidorService.listarSeguidores(this.usuario.getPersona());
		}catch(Exception e) {
			
		}
	}

	public List<PublicadorSeguidor> getSeguidores() {
		return seguidores;
	}

	public void setSeguidores(List<PublicadorSeguidor> seguidores) {
		this.seguidores = seguidores;
	}

		
}
