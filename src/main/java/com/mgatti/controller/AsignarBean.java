package com.mgatti.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.DualListModel;

import com.mgatti.model.Usuario;
import com.mgatti.model.Rol;
import com.mgatti.model.Usuario;
import com.mgatti.service.IRolService;
import com.mgatti.service.IUsuarioRolService;
import com.mgatti.service.IUsuarioService;

@Named
@ViewScoped
public class AsignarBean implements Serializable {

	@Inject
	private IUsuarioService usuarioService;
	@Inject
	private IRolService rolService;
	@Inject
	private IUsuarioRolService usuarioRolService;

	private Usuario usuario;
	private List<Usuario> usuarios;
	private DualListModel<Rol> roles;
	

	@PostConstruct
	public void init() {
		this.usuario = new Usuario();
		this.listarUsuarios();
		this.listarRoles();
	}
	
	public void listarUsuarios() {
		try {
			this.usuarios = this.usuarioService.listar();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void listarRoles() {		
        List<Rol> rolesSource = new ArrayList<Rol>();
        List<Rol> rolesTarget = new ArrayList<Rol>();
        
        try {
			rolesSource = this.rolService.listar();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        this.roles = new DualListModel<Rol>(rolesSource, rolesTarget);
	}
	
	public void asignar() {
		try {
			this.usuarioRolService.asignarRolesUsuario(this.usuario, this.roles.getTarget());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Getter & Setters
	 * 
	 */

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public DualListModel<Rol> getRoles() {
		return roles;
	}

	public void setRoles(DualListModel<Rol> roles) {
		this.roles = roles;
	}

}
