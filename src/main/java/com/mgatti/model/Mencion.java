package com.mgatti.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="menciones")
public class Mencion implements Serializable{
	
	public final static String CHAR = "@"; 
	
	@Id
	@GeneratedValue(generator = "idMencionSeq")
	@SequenceGenerator(name="idMencionSeq", sequenceName="ID_MENCION_SEQ", allocationSize=1)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="id_publicacion", nullable = false)
	private Publicacion publicacion;
	
	@Column(name="texo", length = 50)
	private String texto;
	
	public Mencion() {
	}	

	public Mencion(Publicacion publicacion, String texto) {
		this.publicacion = publicacion;
		this.texto = texto;
	}



	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Publicacion getPublicacion() {
		return publicacion;
	}

	public void setPublicacion(Publicacion publicacion) {
		this.publicacion = publicacion;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}
	
	
}
