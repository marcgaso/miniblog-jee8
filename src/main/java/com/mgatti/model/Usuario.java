package com.mgatti.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.mindrot.jbcrypt.BCrypt;

@Entity
@Table(name="usuarios")
public class Usuario implements Serializable{
	
	@Id
	private Integer id;
	
	@OneToOne(cascade = CascadeType.ALL)
	@MapsId //Indica que la llave primaria tambi�n es for�nea. Va a buscar si en persona habla de usuario
	@JoinColumn(name = "id", nullable = false)
	private Persona persona;
	
	@Column(name="usuario", nullable=false, length = 30)
	private String usuario;
	
	@Column(name="contrase�a", nullable=false, length = 80)
	private String contrase�a = BCrypt.hashpw("*#**___sinpassword**$*___", BCrypt.gensalt());
	
	@Column(name="estado", nullable=false, length = 1)
	private String estado = "A"; //Cuando se cree lo ponga Activo
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Persona getPersona() {
		return persona;
	}
	public void setPersona(Persona persona) {
		this.persona = persona;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getContrase�a() {
		return contrase�a;
	}
	public void setContrase�a(String contrase�a) {		
		this.contrase�a = contrase�a; 
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public void hashContrase�a() {		
		this.contrase�a = BCrypt.hashpw(this.contrase�a, BCrypt.gensalt());; 
	}
	
	
	
}
