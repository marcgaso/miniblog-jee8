package com.mgatti.model;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="personas")
public class Persona implements Serializable{
	
	@Id
	@GeneratedValue(generator = "idPersonaSeq")
	@SequenceGenerator(name="idPersonaSeq", sequenceName="ID_PERSONA_SEQ", allocationSize=1)
	private Integer id;
	
	@OneToOne(cascade = CascadeType.ALL, mappedBy = "persona")
	private Usuario usuario;
	
	@Column(name="nombres", nullable=false, length = 50)
	private String nombres;
	
	@Column(name="apellidos", nullable=false, length = 50)
	private String apellidos;
	
	@Column(name="sexo", nullable=false, length = 1)
	private String sexo;
	
	@Column(name="pais", nullable=false, length = 25)
	private String pais;
	
	@Column(name="direccion", nullable=false, length = 150)
	private String direccion;
	
	@Lob
	private byte[] foto;	
	
	//Para definir un atributo en el modelo que no sea interpretado por JPA como una columna se utiliza el siguiente TAG
	@Transient
	private boolean esSeguido;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public byte[] getFoto() {
		return foto;
	}
	public void setFoto(byte[] foto) {
		this.foto = foto;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public boolean isMale() {
		return Objects.nonNull(this) ? this.sexo.equalsIgnoreCase("M") : false;
	}
	
	public boolean isFemale() {
		return Objects.nonNull(this) ? this.sexo.equalsIgnoreCase("F") : false;
	}
	public boolean isEsSeguido() {
		return esSeguido;
	}
	public void setEsSeguido(boolean esSeguido) {
		this.esSeguido = esSeguido;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Persona other = (Persona) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
