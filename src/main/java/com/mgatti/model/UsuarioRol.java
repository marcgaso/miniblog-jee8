package com.mgatti.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="usuarios_roles")
public class UsuarioRol implements Serializable{

	@Id
	@GeneratedValue(generator = "idUsuarioRolSeq")
	@SequenceGenerator(name="idUsuarioRolSeq", sequenceName="ID_USUARIO_ROL_SEQ", allocationSize=1)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="id_usuario", nullable=false)
	private Usuario usuario;
	
	@ManyToOne
	@JoinColumn(name="id_rol", nullable=false)
	private Rol rol;
	
	public UsuarioRol() {
		
	}
	public UsuarioRol(Usuario u, Rol r) {
		this.usuario = u;
		this.rol = r;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}
	
	
}
