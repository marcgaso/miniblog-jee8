package com.mgatti.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "publicaciones")
public class Publicacion implements Serializable {

	@Id
	@GeneratedValue(generator = "idPublicacionSeq")
	@SequenceGenerator(name = "idPublicacionSeq", sequenceName = "ID_PUBLICACION_SEQ", allocationSize = 1)
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "id_publicador", nullable = false)
	private Persona publicador;

	@Column(name = "cuerpo", nullable = false, length = 250)
	private String cuerpo;

	@OneToMany(mappedBy = "publicacion", cascade = { CascadeType.PERSIST, CascadeType.REMOVE,
			CascadeType.MERGE }, fetch = FetchType.LAZY, orphanRemoval = true)
	private List<Tag> tags;

	@OneToMany(mappedBy = "publicacion", cascade = { CascadeType.PERSIST, CascadeType.REMOVE,
			CascadeType.MERGE }, fetch = FetchType.LAZY, orphanRemoval = true)
	private List<Mencion> menciones;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Persona getPublicador() {
		return publicador;
	}

	public void setPublicador(Persona publicador) {
		this.publicador = publicador;
	}

	public String getCuerpo() {
		return cuerpo;
	}

	public void setCuerpo(String cuerpo) {
		this.cuerpo = cuerpo;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public List<Mencion> getMenciones() {
		return menciones;
	}

	public void setMenciones(List<Mencion> menciones) {
		this.menciones = menciones;
	}
	
	public void buildTagsMenciones() {
		this.tags = new ArrayList<>();
		this.menciones = new ArrayList<>();
		
		String texto = this.getCuerpo().replace(',',' ').replace('.',' ');
		
		
		for(String e: texto.split(" "))
			if(e.startsWith(Tag.CHAR))
				this.tags.add(new Tag(this,e.substring(1)));
			else if (e.startsWith(Mencion.CHAR))
				this.menciones.add(new Mencion(this,e.substring(1)));
		
		
	}
	
	
}
