package com.mgatti.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="publicadores_seguidores")
public class PublicadorSeguidor implements Serializable{

	@Id
	@GeneratedValue(generator = "idPublicadorSeguidorSeq")
	@SequenceGenerator(name="idPublicadorSeguidorSeq", sequenceName="ID_PUBLICADOR_SEGUIDOR_SEQ", allocationSize=1)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="id_publicador", nullable=false)
	private Persona publicador;
	
	@ManyToOne
	@JoinColumn(name="id_seguidor", nullable=false)
	private Persona seguidor;
	
	@Column(name="fecha", nullable=false)
	private LocalDateTime fecha;

	public PublicadorSeguidor() {
	}

	public PublicadorSeguidor(Persona publicador, Persona seguidor) {
		this.publicador = publicador;
		this.seguidor = seguidor;
		this.fecha = LocalDateTime.now();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Persona getPublicador() {
		return publicador;
	}

	public void setPublicador(Persona publicador) {
		this.publicador = publicador;
	}

	public Persona getSeguidor() {
		return seguidor;
	}

	public void setSeguidor(Persona seguidor) {
		this.seguidor = seguidor;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}
	
	
}
